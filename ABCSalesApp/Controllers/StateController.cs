﻿using ABCSalesApp.Extension.AppCustom;
using ABCSalesApp.Models.ViewModels;
using ABCSalesApp.Repositories;
using System;
using System.Data;
using System.Web.Mvc;

namespace ABCSalesApp.Controllers
{
    public class StateController : Controller
    {
        private StateRepository _repository { get; set; }
        private SelectItemReposity _selectHelper { get; set; }

        public StateController()
        {
            _repository = new StateRepository();
            _selectHelper = new SelectItemReposity();
        }

        // GET: State
        public ActionResult Index(int? currentPage)
        {
             var datas = _repository.GetGridDatas(!currentPage.HasValue ? 1 : currentPage.Value);
             return View(datas);
        }

        // GET: State/Details/5
        public ActionResult Details(string id)
        {
            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Index");

            return View(_repository.GetDetailById(id));
        }

        // GET: State/Create
        public ActionResult Create()
        {
            var data = new StateModifyViewModels();
            data.Countries = _selectHelper.GetCoutries();

            return View(data);
        }

        // POST: State/Create
        [HttpPost]
        public ActionResult Create(StateModifyViewModels data)
        {
            data.Countries = _selectHelper.GetCoutries(data.CountryId);

            try
            {
                if (!ModelState.IsValid)
                {
                    return View(data);
                }
                // TODO: Add insert logic here
                _repository.SaveNewRecord(data);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ex.LogError();
                return View(data);
            }
        }

        // GET: State/Edit/5
        public ActionResult Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Index");

            var data = _repository.GetDetailToEditById(id);

            if (data == null)
            {
                return RedirectToAction("Index");
            }

            data.Countries = _selectHelper.GetCoutries(data.CountryId);

            return View(data);
        }

        // POST: State/Edit/5
        [HttpPost]
        public ActionResult Edit(StateModifyViewModels data)
        {
            data.Countries = _selectHelper.GetCoutries(data.CountryId);

            try
            {
                if (!ModelState.IsValid)
                {
                    return View(data);
                }

                // TODO: Add update logic here
                _repository.UpdateRecord(data);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ex.LogError();
                return View(data);
            }
        }

        // GET: State/Delete/5
        public ActionResult Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Index");

            var data = _repository.GetDetailById(id);

            if (data == null)
            {
                return RedirectToAction("Index");
            }

            return View(data);
        }

        // POST: State/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(string id)
        {
            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Index");

            try
            {
                // TODO: Add delete logic here
                _repository.DeleteRecord(id);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ex.LogError();
                return View();
            }
        }
    }
}
