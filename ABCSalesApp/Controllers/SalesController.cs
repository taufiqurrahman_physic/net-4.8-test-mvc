﻿using ABCSalesApp.Extension.AppCustom;
using ABCSalesApp.Models.ViewModels;
using ABCSalesApp.Repositories;
using System;
using System.Reflection;
using System.Web.Mvc;

namespace ABCSalesApp.Controllers
{
    public class SalesController : Controller
    {
        private SalesRepository _repository { get;set; }
        private SelectItemReposity _selectHelper { get; set; }
        public SalesController()
        {
            _repository = new SalesRepository();
            _selectHelper = new SelectItemReposity();
        }
        // GET: Sales
        public ActionResult Index()
        {
            var data = TempData["parameter"];

            if(data != null)
            {
               
                var result = _repository.GetGridDatas((SalesSearchParameter)data);
                result.SalesSearchParameter.Countries = _selectHelper.GetCoutries(result.SalesSearchParameter.CountryId);
                result.SalesSearchParameter.States = _selectHelper.GetStates(result.SalesSearchParameter.CountryId, result.SalesSearchParameter.StateId);
                result.SalesSearchParameter.Cities = _selectHelper.GetCities(result.SalesSearchParameter.CountryId, result.SalesSearchParameter.StateId, result.SalesSearchParameter.CityId);
                return View(result);
            }

            var datas = _repository.GetGridDatas(new SalesSearchParameter
            {
                CurrentPage = 1,
                Countries = _selectHelper.GetCoutries()
            });

            return View(datas);
        }

        [HttpPost]
        public ActionResult SearchSales(SalesSearchParameter data)
        {
            if(data == null)
                return RedirectToAction("Index");

            TempData["parameter"] = data;

            return RedirectToAction("Index");
        }
        // GET: Sales/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Sales/Create
        public ActionResult Create()
        {
            var model = new SalesModifyViewModels();
            model.Products = _selectHelper.GetProducts();
            model.Countries = _selectHelper.GetCoutries();

            return View(model);
        }

        // POST: Sales/Create
        [HttpPost]
        public ActionResult Create(SalesModifyViewModels data)
        {
            data.Products = _selectHelper.GetProducts(data.ProductId);
            data.Countries = _selectHelper.GetCoutries(data.CountryId);
            data.States = _selectHelper.GetStates(data.CountryId, data.StateId);
            data.Cities = _selectHelper.GetCities(data.CountryId, data.StateId, data.CityId);
            
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(data);
                }

                // TODO: Add insert logic here
                _repository.SaveNewRecord(data);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ex.LogError();
                return View(data);
            }
        }

        // GET: Sales/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Sales/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Sales/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Sales/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
