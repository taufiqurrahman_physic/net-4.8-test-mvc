﻿using ABCSalesApp.Extension.AppCustom;
using ABCSalesApp.Models.ViewModels;
using ABCSalesApp.Repositories;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ABCSalesApp.Controllers
{
    public class CountryController : Controller
    {
        private CountryRepository _repository { get; set; }

        public CountryController()
        {
            _repository = new CountryRepository();
        }

        // GET: Country
        public ActionResult Index(int? currentPage)
        {
            return View(_repository.GetGridDatas(!currentPage.HasValue ? 1 : currentPage.Value));
        }

        // GET: Country/Details/5
        public ActionResult Details(string id)
        {
            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Index");

            return View(_repository.GetDetailById(id));
        }

        // GET: Country/Create
        public ActionResult Create()
        {
            return View(new CountryModifyViewModels());
        }

        // POST: Country/Create
        [HttpPost]
        public ActionResult Create(CountryModifyViewModels data)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(data);
                }
                // TODO: Add insert logic here
                _repository.SaveNewRecord(data);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ex.LogError();
                return View(data);
            }
        }

        // GET: Country/Edit/5
        public ActionResult Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Index");

            var data = _repository.GetDetailToEditById(id);

            if(data == null)
            {
                return RedirectToAction("Index");
            }

            return View(data);
        }

        // POST: Country/Edit/5
        [HttpPost]
        public ActionResult Edit(CountryModifyViewModels data)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(data);
                }
                // TODO: Add update logic here
                _repository.UpdateRecord(data);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ex.LogError();
                return View(data);
            }
        }

        // GET: Country/Delete/5
        public ActionResult Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Index");

            var data = _repository.GetDetailById(id);

            if (data == null)
            {
                return RedirectToAction("Index");
            }

            return View(data);
        }

        // POST: Country/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(string id)
        {
            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Index");

            try
            {
                // TODO: Add delete logic here
                _repository.DeleteRecord(id);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ex.LogError();
                return View(new CountryDetailViewModels());
            }
        }
    }
}
