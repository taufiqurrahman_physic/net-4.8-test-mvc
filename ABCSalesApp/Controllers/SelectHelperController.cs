﻿using ABCSalesApp.Repositories;
using System.Web.Mvc;

namespace ABCSalesApp.Controllers
{
    public class SelectHelperController : Controller
    {
        private SelectItemReposity _selectHelper { get; set; }

        public SelectHelperController()
        {
            _selectHelper = new SelectItemReposity();
        }
        public JsonResult GetSelectItemStates(string countryId)
        {
            var states = _selectHelper.GetStates(countryId);

            return Json(states, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSelectItemCities(string countryId, string stateId)
        {
            var states = _selectHelper.GetCities(countryId, stateId);

            return Json(states, JsonRequestBehavior.AllowGet);
        }
    }
}