﻿using ABCSalesApp.Extension.AppCustom;
using ABCSalesApp.Models;
using ABCSalesApp.Models.ViewModels;
using ABCSalesApp.Repositories;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace ABCSalesApp.Controllers
{
    public class CityController : Controller
    {
        private CityRepository _repository {  get; set; }
        private SelectItemReposity _selectHelper { get; set; }

        public CityController()
        {
            _repository = new CityRepository();
            _selectHelper = new SelectItemReposity();
        }

        // GET: City
        public ActionResult Index(int? currentPage)
        {
            return View(_repository.GetGridDatas(!currentPage.HasValue ? 1 : currentPage.Value));
        }

        // GET: City/Details/5
        public ActionResult Details(string id)
        {
            if(string.IsNullOrEmpty(id))
                return RedirectToAction("Index");

            return View(_repository.GetDetailById(id));
        }

        // GET: City/Create
        public ActionResult Create()
        {
            var data = new CityModifyViewModels();
            data.Countries = _selectHelper.GetCoutries();

            return View(data);
        }


        // POST: City/Create
        [HttpPost]
        public ActionResult Create(CityModifyViewModels data)
        {
            data.Countries = _selectHelper.GetCoutries(data.CountryId);
            data.States = _selectHelper.GetStates(data.CountryId,data.StateId);

            try
            {
                if (!ModelState.IsValid)
                {
                    return View(data);
                }

                // TODO: Add insert logic here
                _repository.SaveNewRecord(data);

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ex.LogError();

                return View(data);
            }
        }

        // GET: City/Edit/5
        public ActionResult Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Index");

            var data = _repository.GetDetailToEditById(id);

            if (data == null)
            {
                return RedirectToAction("Index");
            }

            data.Countries = _selectHelper.GetCoutries(data.CountryId);
            data.States = _selectHelper.GetStates(data.CountryId, data.StateId);

            return View(data);
        }

        // POST: City/Edit/5
        [HttpPost]
        public ActionResult Edit(CityModifyViewModels data)
        {
            data.Countries = _selectHelper.GetCoutries(data.CountryId);
            data.States = _selectHelper.GetStates(data.CountryId, data.StateId);

            try
            {
                if (!ModelState.IsValid)
                {
                    return View(data);
                }
                // TODO: Add update logic here
                _repository.UpdateRecord(data);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ex.LogError();

                return View(data);
            }
        }

        // GET: City/Delete/5
        public ActionResult Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Index");

            var data = _repository.GetDetailById(id);

            if (data == null)
            {
                return RedirectToAction("Index");
            }

            return View(data);
        }

        // POST: City/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(string id)
        {
            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Index");

            try
            {
                // TODO: Add delete logic here
                _repository.DeleteRecord(id);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ex.LogError();
                return View(new CityDetailViewModels());
            }
        }
    }
}
