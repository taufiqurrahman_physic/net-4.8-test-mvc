﻿using ABCSalesApp.Extension.AppCustom;
using System.Web;
using System.Web.Mvc;

namespace ABCSalesApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CustomExceptionHandlerAttribute());
        }
    }
}
