﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace ABCSalesApp.Extension.Converter
{
    public static class TimeConverter
    {
        public static DateTime LocalToUTC(this DateTime localDateTime)
        {
            TimeZoneInfo localTimeZone = TimeZoneInfo.Local;
            DateTime utcTime = TimeZoneInfo.ConvertTimeToUtc(localDateTime, localTimeZone);
            return utcTime;
        }

        public static DateTime UTCToLocal(this DateTime utcDateTime)
        {
            TimeZoneInfo localTimeZone = TimeZoneInfo.Local;
            DateTime localTime = TimeZoneInfo.ConvertTimeFromUtc(utcDateTime, localTimeZone);
            return localTime;
        }
    }
}