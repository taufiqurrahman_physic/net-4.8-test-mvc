﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCSalesApp.Extension.Converter
{
    public static class ObjectToDataType
    {
        public static int ObjectToInt(this object input)
        {
            if (input == null)
            {
                return 0;
            }

            int result = 0;

            try
            {
               result = Convert.ToInt32(input);  
            }
            catch(Exception ex)
            {

            }

            return result;
        }

        public static string ObjectToString(this object input)
        {
            if (input == null)
            {
                return string.Empty;
            }

            string result = string.Empty;

            try
            {
                result = input.ToString();
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public static DateTime ObjectToDate(this object input)
        {
            if (input == null)
            {
                return DateTime.MinValue;
            }

            try
            {
                return Convert.ToDateTime(input).UTCToLocal();
            }
            catch (Exception ex)
            {

            }

            return DateTime.MinValue;
        }
    }
}