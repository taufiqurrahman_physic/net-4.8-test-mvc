﻿
using ABCSalesApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCSalesApp.Extension.Converter
{
    public static class JsonConverter
    {
        public static string GetJsonFromObject<T>(this T data) where T : class
        {
            string result = null;
            if (data == null)
                return result;
            try
            {
                result = JsonConvert.SerializeObject(data);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }

        public static T GetObjectFromJson<T>(this string data) where T : class
        {
            T result = null;
            try
            {
                if (!string.IsNullOrEmpty(data))
                    result = JsonConvert.DeserializeObject<T>(data);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            }
            return result;
        }

    }
}