﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ABCSalesApp.Extension.AdditionalAttribute
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class IntGreatherThanAttribute : ValidationAttribute
    {
        private readonly string _otherPropertyName;

        public IntGreatherThanAttribute(string otherPropertyName)
        {
            _otherPropertyName = otherPropertyName;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var otherPropertyInfo = validationContext.ObjectType.GetProperty(_otherPropertyName);

            if (otherPropertyInfo == null)
            {
                return new ValidationResult($"Property {_otherPropertyName} not found.");
            }

            var otherValue = otherPropertyInfo.GetValue(validationContext.ObjectInstance);

            if (value != null && otherValue != null && (int)value >= (int)otherValue)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult($"{validationContext.DisplayName} must be greater than {_otherPropertyName}.");
        }
    }
}