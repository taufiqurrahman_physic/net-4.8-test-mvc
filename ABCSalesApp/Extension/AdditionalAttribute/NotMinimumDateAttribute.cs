﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ABCSalesApp.Extension.AdditionalAttribute
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class NotMinimumDateAttribute : ValidationAttribute
    {
        private readonly string _message;

        public NotMinimumDateAttribute(string message)
        {
            _message = message;
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value is DateTime dateTime)
            {
                if( dateTime > DateTime.MinValue)
                {
                    return ValidationResult.Success;
                }
            }

            return new ValidationResult(_message); // Return true for non-DateTime values
        }
    }
}