﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCSalesApp.Extension
{
    public class BaseDataEntityModel
    {
        public string Id { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedString { get; set; }
        
    }
}