﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace ABCSalesApp.Extension.AppCustom
{
    public static class LogErrorHandler
    {
        public static void LogError( this Exception ex)
        {
            try
            {
                var path = ConfigurationManager.AppSettings["LogFilePath"];

                if (string.IsNullOrEmpty(path))
                {
                    // Handle missing log file path configuration
                    Console.WriteLine("Error: Log file path is not configured.");
                    return;
                }

                var fileName = $"Log Error - {DateTime.Now.Ticks}.txt";
                var fullPath = Path.Combine(path, fileName);

                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine($"[{DateTime.Now}] Error: {ex.Message}");
                    writer.WriteLine($"StackTrace: {ex.StackTrace}");
                    writer.WriteLine("-------------------------------------------------------------");
                }
            }
            catch (Exception logException)
            {
                // Handle the exception while logging (optional)
                Console.WriteLine($"Error logging exception: {logException.Message}");
            }
        }
    }
}