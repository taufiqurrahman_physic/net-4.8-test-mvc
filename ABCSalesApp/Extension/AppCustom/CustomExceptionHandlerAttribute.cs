﻿using ABCSalesApp.Models.ViewModels;
using ABCSalesApp.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABCSalesApp.Extension.AppCustom
{
    public class CustomExceptionHandlerAttribute : FilterAttribute, IExceptionFilter
    {
        private ErrorHandlerRepository _eerorRepos { get; set; }

        public CustomExceptionHandlerAttribute()
        {
            _eerorRepos = new ErrorHandlerRepository();
        }

        public void OnException(ExceptionContext filterContext)
        {
            AppErrorLogger logger = new AppErrorLogger()
            {
                ExceptionMessage = filterContext.Exception.Message,
                ExceptionStackTrack = filterContext.Exception.StackTrace,
                ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                ActionName = filterContext.RouteData.Values["action"].ToString(),
                ExceptionLogTime = DateTime.Now
            };

            _eerorRepos.SaveNewRecord(logger);

            filterContext.ExceptionHandled = true;
            filterContext.Result = new ViewResult()
            {
                ViewName = "Error"
            };
        }
    }
}