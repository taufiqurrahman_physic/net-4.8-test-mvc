﻿using ABCSalesApp.Extension.AppCustom;
using ABCSalesApp.Extension.Converter;
using ABCSalesApp.Models;
using ABCSalesApp.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ABCSalesApp.Repositories
{
    public class CountryRepository : BaseRepository
    {
        public PaginationAndDatasViewModels<CountryDetailViewModels> GetGridDatas(int currentPage)
        {
            var result = new PaginationAndDatasViewModels<CountryDetailViewModels>();
            result.CurrentPages = currentPage;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("GetCountriesGridData", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@current_page", currentPage);
                    cmd.Parameters.AddWithValue("@page_size", result.TotalDataInPages);

                    // Execute the command
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        // Process the first result set
                        while (reader.Read())
                        {
                            result.TotalDatas = reader["TotalData"].ObjectToInt();
                        }

                        // Move to the next result set
                        reader.NextResult();

                        List<CountryDetailViewModels> countries = new List<CountryDetailViewModels>();
                        // Process the second result set
                        while (reader.Read())
                        {
                            countries.Add(new CountryDetailViewModels
                            {
                                Id = reader["Id"].ObjectToString(),
                                Name = reader["Name"].ObjectToString(),
                                CallingCode = reader["CallingCode"].ObjectToString(),
                                Code = reader["Code"].ObjectToString(),
                                CreatedBy = reader["CreatedBy"].ObjectToString(),
                                CreatedDate = reader["CreatedDate"].ObjectToDate()
                            });
                        }

                        result.GetDatas = countries;
                    }
                }
            }

            return result;
        }

        public CountryDetailViewModels GetDetailById(string id)
        {
            var result = new CountryDetailViewModels();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("GetCountryById", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@countryId", id);

                    // Execute the command
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        
                        // Process the second result set
                        while (reader.Read())
                        {
                            result.Id = reader["Id"].ObjectToString();
                            result.Name = reader["Name"].ObjectToString();
                            result.CallingCode = reader["CallingCode"].ObjectToString();
                            result.Code = reader["Code"].ObjectToString();
                            result.CreatedBy = reader["CreatedBy"].ObjectToString();
                            result.CreatedDate = reader["CreatedDate"].ObjectToDate();
                        }
                    }
                }
            }

            return result;
        }

        public CountryModifyViewModels GetDetailToEditById(string id)
        {
            var result = new CountryModifyViewModels();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("GetCountryById", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@countryId", id);

                    // Execute the command
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        // Process the second result set
                        while (reader.Read())
                        {
                            result.Id = reader["Id"].ObjectToString();
                            result.Name = reader["Name"].ObjectToString();
                            result.CallingCode = reader["CallingCode"].ObjectToString();
                            result.Code = reader["Code"].ObjectToString();
                        }
                    }
                }
            }

            return result;
        }

        public bool SaveNewRecord(CountryModifyViewModels data)
        {
            var result = false;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("InsertNewCountry", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    Country dataToInsert = new Country
                    {
                        CallingCode = data.CallingCode,
                        UpdatedString = "New Record Created By Admin",
                        Code = data.Code,
                        CreatedBy = "Admin",
                        CreatedDate = DateTime.Now.LocalToUTC(),
                        Id = Guid.NewGuid().ToString(),
                        Name = data.Name
                    };

                    cmd.Parameters.AddWithValue("@jsonData", dataToInsert.GetJsonFromObject());

                    // Execute the command
                    try
                    {
                        cmd.ExecuteNonQuery();

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                    }
                }
            }

            return result;
        }

        public bool UpdateRecord(CountryModifyViewModels data)
        {
            var result = false;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("UpdateCountry", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@id", data.Id);
                    cmd.Parameters.AddWithValue("@UpdatedBy", "Admin");
                    cmd.Parameters.AddWithValue("@updatedDate", DateTime.Now.LocalToUTC());
                    cmd.Parameters.AddWithValue("@updateString", "Updated By Admin");
                    cmd.Parameters.AddWithValue("@name", data.Name);
                    cmd.Parameters.AddWithValue("@code", data.Code);
                    cmd.Parameters.AddWithValue("@callingCode", data.CallingCode);

                    // Execute the command
                    try
                    {
                        cmd.ExecuteNonQuery();

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                    }
                }
            }

            return result;
        }

        public bool DeleteRecord(string id)
        {
            var result = false;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("DeleteCountry", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@id", id);

                    // Execute the command
                    try
                    {
                        cmd.ExecuteNonQuery();

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                    }
                }
            }

            return result;
        }
    }
    
}