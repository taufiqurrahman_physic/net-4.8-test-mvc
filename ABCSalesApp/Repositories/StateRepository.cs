﻿using ABCSalesApp.Extension.AppCustom;
using ABCSalesApp.Extension.Converter;
using ABCSalesApp.Models;
using ABCSalesApp.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;
using System.Xml.Linq;

namespace ABCSalesApp.Repositories
{
    public class StateRepository : BaseRepository
    {
        public PaginationAndDatasViewModels<StateDetailViewModels> GetGridDatas(int currentPage)
        {
            var result = new PaginationAndDatasViewModels<StateDetailViewModels>();
            result.CurrentPages = currentPage;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("GetStatesGridData", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@current_page", currentPage);
                    cmd.Parameters.AddWithValue("@page_size", result.TotalDataInPages);

                    // Execute the command
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        // Process the first result set
                        while (reader.Read())
                        {
                            result.TotalDatas = reader["TotalData"].ObjectToInt();
                        }

                        // Move to the next result set
                        reader.NextResult();

                        List<StateDetailViewModels> datas = new List<StateDetailViewModels>();
                        // Process the second result set
                        while (reader.Read())
                        {
                            datas.Add(new StateDetailViewModels
                            {
                                Id = reader["Id"].ObjectToString(),
                                Name = reader["Name"].ObjectToString(),
                                AreaCode = reader["AreaCode"].ObjectToString(),
                                StateCode = reader["StateCode"].ObjectToString(),
                                CountryName = reader["CountryName"].ObjectToString(),
                                CreatedBy = reader["CreatedBy"].ObjectToString(),
                                CreatedDate = reader["CreatedDate"].ObjectToDate()
                            });
                        }

                        result.GetDatas = datas;
                    }
                }
            }

            return result;
        }

        public StateDetailViewModels GetDetailById(string id)
        {
            var result = new StateDetailViewModels();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("GetStateById", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@stateId", id);

                    // Execute the command
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        // Process the second result set
                        while (reader.Read())
                        {
                            result.Id = reader["Id"].ObjectToString();
                            result.Name = reader["Name"].ObjectToString();
                            result.AreaCode = reader["AreaCode"].ObjectToString();
                            result.StateCode = reader["StateCode"].ObjectToString();
                            result.CountryName = reader["CountryName"].ObjectToString();
                            result.CreatedBy = reader["CreatedBy"].ObjectToString();
                            result.CreatedDate = reader["CreatedDate"].ObjectToDate();
                        }
                    }
                }
            }

            return result;
        }

        public StateModifyViewModels GetDetailToEditById(string id)
        {
            var result = new StateModifyViewModels();
            result.Countries = new List<SelectListItem>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("GetStateById", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@stateId", id);

                    // Execute the command
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        // Process the second result set
                        while (reader.Read())
                        {
                            result.Id = reader["Id"].ObjectToString();
                            result.Name = reader["Name"].ObjectToString();
                            result.AreaCode = reader["AreaCode"].ObjectToString();
                            result.StateCode = reader["StateCode"].ObjectToString();
                            result.CountryId = reader["CountryId"].ObjectToString();
                        }
                    }
                }
            }

            return result;
        }

        public bool SaveNewRecord(StateModifyViewModels data)
        {
            var result = false;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("InsertNewState", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    State dataToInsert = new State
                    {
                        StateCode = data.StateCode,
                        UpdatedString = "New Record Created By Admin",
                        AreaCode = data.AreaCode,
                        CountryId = data.CountryId,
                        CreatedBy = "Admin",
                        CreatedDate = DateTime.Now.LocalToUTC(),
                        Id = Guid.NewGuid().ToString(),
                        Name = data.Name
                    };

                    cmd.Parameters.AddWithValue("@jsonData", dataToInsert.GetJsonFromObject());

                    // Execute the command
                    try
                    {
                        cmd.ExecuteNonQuery();

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                    }
                }
            }

            return result;
        }

        public bool UpdateRecord(StateModifyViewModels data)
        {
            var result = false;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("UpdateState", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@id", data.Id);
                    cmd.Parameters.AddWithValue("@UpdatedBy", "Admin");
                    cmd.Parameters.AddWithValue("@updatedDate", DateTime.Now.LocalToUTC());
                    cmd.Parameters.AddWithValue("@updateString", "Updated By Admin");
                    cmd.Parameters.AddWithValue("@name", data.Name);
                    cmd.Parameters.AddWithValue("@countryId", data.CountryId);
                    cmd.Parameters.AddWithValue("@areaCode", data.AreaCode);
                    cmd.Parameters.AddWithValue("@stateCode", data.StateCode);

                    // Execute the command
                    try
                    {
                        cmd.ExecuteNonQuery();

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                    }
                }
            }

            return result;
        }

        public bool DeleteRecord(string id)
        {
            var result = false;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("DeleteState", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@id", id);

                    // Execute the command
                    try
                    {
                        cmd.ExecuteNonQuery();

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                    }
                }
            }

            return result;
        }
    }
    
}