﻿using ABCSalesApp.Models.ViewModels;
using ABCSalesApp.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using ABCSalesApp.Extension.Converter;
using ABCSalesApp.Extension.AppCustom;

namespace ABCSalesApp.Repositories
{
    public class ErrorHandlerRepository: BaseRepository
    {
        public bool SaveNewRecord(AppErrorLogger data)
        {
            var result = false;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("InsertNewError", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;
                    
                    data.Id = Guid.NewGuid().ToString();
                    data.ExceptionLogTime = DateTime.Now.LocalToUTC();

                    cmd.Parameters.AddWithValue("@jsonData", data.GetJsonFromObject());

                    // Execute the command
                    try
                    {
                        cmd.ExecuteNonQuery();

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                    }
                }
            }

            return result;
        }
    }
}