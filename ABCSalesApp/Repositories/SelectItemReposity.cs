﻿using ABCSalesApp.Extension.Converter;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;

namespace ABCSalesApp.Repositories
{
    public class SelectItemReposity : BaseRepository
    {
        public IEnumerable<SelectListItem> GetCoutries(string selectedId = null)
        {
            var result = new List<SelectListItem>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("GetDropdownForCountry", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Execute the command
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        // Process the second result set
                        while (reader.Read())
                        {
                            result.Add(new SelectListItem
                            {
                                Text = reader["Name"].ObjectToString(),
                                Value = reader["Id"].ObjectToString(),
                                Selected = reader["Id"].ObjectToString() == selectedId
                            });
                        }
                    }
                }
            }

            return result;
        }

        public IEnumerable<SelectListItem> GetCities(string countryId, string stateId, string selectedId = null)
        {
            var result = new List<SelectListItem>();

            if(string.IsNullOrEmpty(countryId) || string.IsNullOrEmpty(stateId))
                return result;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("GetDropdownForCity", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@stateId", stateId);
                    cmd.Parameters.AddWithValue("@countryId", countryId);

                    // Execute the command
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        // Process the second result set
                        while (reader.Read())
                        {
                            result.Add(new SelectListItem
                            {
                                Text = reader["Name"].ObjectToString(),
                                Value = reader["Id"].ObjectToString(),
                                Selected = reader["Id"].ObjectToString() == selectedId
                            });
                        }
                    }
                }
            }

            return result;
        }

        public IEnumerable<SelectListItem> GetStates(string countryId, string selectedId = null)
        {
            var result = new List<SelectListItem>();

            if(string.IsNullOrEmpty(countryId))
                return result;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("GetDropdownForState", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@countryId", countryId);

                    // Execute the command
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        // Process the second result set
                        while (reader.Read())
                        {
                            result.Add(new SelectListItem
                            {
                                Text = reader["Name"].ObjectToString(),
                                Value = reader["Id"].ObjectToString(),
                                Selected = reader["Id"].ObjectToString() == selectedId
                            });
                        }
                    }
                }
            }

            return result;
        }

        public IEnumerable<SelectListItem> GetProducts( string selectedId = null)
        {
            var result = new List<SelectListItem>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("GetDropdownForProduct", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    // Execute the command
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        // Process the second result set
                        while (reader.Read())
                        {
                            result.Add(new SelectListItem
                            {
                                Text = reader["Name"].ObjectToString(),
                                Value = reader["Id"].ObjectToString(),
                                Selected = reader["Id"].ObjectToString() == selectedId
                            });
                        }
                    }
                }
            }

            return result;
        }
    }
}