﻿using ABCSalesApp.Extension.AppCustom;
using ABCSalesApp.Extension.Converter;
using ABCSalesApp.Models;
using ABCSalesApp.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Linq;

namespace ABCSalesApp.Repositories
{
    public class ProductRepository : BaseRepository
    {
        public PaginationAndDatasViewModels<ProductDetailViewModels> GetGridDatas(int currentPage)
        {
            var result = new PaginationAndDatasViewModels<ProductDetailViewModels>();
            result.CurrentPages = currentPage;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("GetProductsGridData", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@current_page", currentPage);
                    cmd.Parameters.AddWithValue("@page_size", result.TotalDataInPages);

                    // Execute the command
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        // Process the first result set
                        while (reader.Read())
                        {
                            result.TotalDatas = reader["TotalData"].ObjectToInt();
                        }

                        // Move to the next result set
                        reader.NextResult();

                        List<ProductDetailViewModels> datas = new List<ProductDetailViewModels>();
                        // Process the second result set
                        while (reader.Read())
                        {
                            datas.Add(new ProductDetailViewModels
                            {
                                Id = reader["Id"].ObjectToString(),
                                Name = reader["Name"].ObjectToString(),
                                TotalQuantity = reader["TotalQuantity"].ObjectToInt(),
                                StoredQuantity = reader["StoredQuantity"].ObjectToInt(),
                                CreatedBy = reader["CreatedBy"].ObjectToString(),
                                CreatedDate = reader["CreatedDate"].ObjectToDate()
                            });
                        }

                        result.GetDatas = datas;
                    }
                }
            }

            return result;
        }

        public ProductDetailViewModels GetDetailById(string id)
        {
            var result = new ProductDetailViewModels();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("GetProductById", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@productId", id);

                    // Execute the command
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        // Process the second result set
                        while (reader.Read())
                        {
                            result.Id = reader["Id"].ObjectToString();
                            result.Name = reader["Name"].ObjectToString();
                            result.TotalQuantity = reader["TotalQuantity"].ObjectToInt();
                            result.StoredQuantity = reader["StoredQuantity"].ObjectToInt();
                            result.CreatedBy = reader["CreatedBy"].ObjectToString();
                            result.CreatedDate = reader["CreatedDate"].ObjectToDate();
                        }
                    }
                }
            }

            return result;
        }

        public ProductModifyViewModels GetDetailToEditById(string id)
        {
            var result = new ProductModifyViewModels();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("GetProductById", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@productId", id);

                    // Execute the command
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        // Process the second result set
                        while (reader.Read())
                        {
                            result.Id = reader["Id"].ObjectToString();
                            result.Name = reader["Name"].ObjectToString();
                            result.TotalQuantity = reader["TotalQuantity"].ObjectToInt();
                            result.StoredQuantity = reader["StoredQuantity"].ObjectToInt();
                        }
                    }
                }
            }

            return result;
        }

        //
        public bool SaveNewRecord(ProductModifyViewModels data)
        {
            var result = false;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("InsertNewProductData", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    Product dataToInsert = new Product
                    {
                        StoredQuantity = data.StoredQuantity,
                        UpdatedString = "New Record Created By Admin",
                        TotalQuantity = data.TotalQuantity,
                        CreatedBy = "Admin",
                        CreatedDate = DateTime.Now.LocalToUTC(),
                        Id = Guid.NewGuid().ToString(),
                        Name = data.Name
                    };

                    cmd.Parameters.AddWithValue("@jsonData", dataToInsert.GetJsonFromObject());

                    // Execute the command
                    try
                    {
                        cmd.ExecuteNonQuery();

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                    }
                }
            }

            return result;
        }

        public bool UpdateRecord(ProductModifyViewModels data)
        {
            var result = false;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("UpdateProduct", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@id", data.Id);
                    cmd.Parameters.AddWithValue("@UpdatedBy", "Admin");
                    cmd.Parameters.AddWithValue("@updatedDate", DateTime.Now.LocalToUTC());
                    cmd.Parameters.AddWithValue("@updateString", "Updated By Admin");
                    cmd.Parameters.AddWithValue("@name", data.Name);
                    cmd.Parameters.AddWithValue("@totalQuantity", data.TotalQuantity);
                    cmd.Parameters.AddWithValue("@storedQuantity", data.StoredQuantity);

                    // Execute the command
                    try
                    {
                        cmd.ExecuteNonQuery();

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                    }
                }
            }

            return result;
        }

        public bool DeleteRecord(string id)
        {
            var result = false;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("DeleteProduct", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@id", id);

                    // Execute the command
                    try
                    {
                        cmd.ExecuteNonQuery();

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                    }
                }
            }

            return result;
        }
    }
    
}