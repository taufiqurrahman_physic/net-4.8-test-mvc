﻿using ABCSalesApp.Extension.AppCustom;
using ABCSalesApp.Extension.Converter;
using ABCSalesApp.Models;
using ABCSalesApp.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;

namespace ABCSalesApp.Repositories
{
    public class SalesRepository :BaseRepository
    {
        public SalesPaginationAndDatasViewModels<SalesDetailViewModels> GetGridDatas(SalesSearchParameter data)
        {
            var result = new SalesPaginationAndDatasViewModels<SalesDetailViewModels>();
            result.CurrentPages = data.CurrentPage;
            result.SalesSearchParameter = data;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("GetSalesInformationGridData", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@current_page", data.CurrentPage);
                    cmd.Parameters.AddWithValue("@page_size", result.TotalDataInPages);

                    if(result.SalesSearchParameter != null)
                    {
                        if (result.SalesSearchParameter.MinimumSalesDate.HasValue)
                            cmd.Parameters.AddWithValue("@min_sale_date", result.SalesSearchParameter.MinimumSalesDate.Value.LocalToUTC());

                        if (result.SalesSearchParameter.MaximumSalesDate.HasValue)
                            cmd.Parameters.AddWithValue("@max_sale_date", result.SalesSearchParameter.MaximumSalesDate.Value.LocalToUTC());

                        if (!string.IsNullOrEmpty(result.SalesSearchParameter.CountryId))
                            cmd.Parameters.AddWithValue("@countryId", result.SalesSearchParameter.CountryId);

                        if (!string.IsNullOrEmpty(result.SalesSearchParameter.StateId))
                            cmd.Parameters.AddWithValue("@stateId", result.SalesSearchParameter.StateId);

                        if (!string.IsNullOrEmpty(result.SalesSearchParameter.CityId))
                            cmd.Parameters.AddWithValue("@cityId", result.SalesSearchParameter.CityId);
                    }
                    
                    // Execute the command
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        // Process the first result set
                        while (reader.Read())
                        {
                            result.TotalDatas = reader["TotalData"].ObjectToInt();
                        }

                        // Move to the next result set
                        reader.NextResult();

                        List<SalesDetailViewModels> datas = new List<SalesDetailViewModels>();
                        // Process the second result set
                        while (reader.Read())
                        {
                            datas.Add(new SalesDetailViewModels
                            {
                                Id = reader["Id"].ObjectToString(),
                                CustomerName = reader["CustomerName"].ObjectToString(),
                                StateName = reader["StateName"].ObjectToString(),
                                CountryName = reader["CountryName"].ObjectToString(),
                                DateAndTimeOfSale = reader["DateAndTimeOfSale"].ObjectToDate(),
                                CityName = reader["CityName"].ObjectToString(),
                                ProductName = reader["ProductName"].ObjectToString(),
                                Quantity = reader["Quantity"].ObjectToInt(),
                                CreatedBy = reader["CreatedBy"].ObjectToString(),
                                CreatedDate = reader["CreatedDate"].ObjectToDate()
                            });
                        }

                        result.GetDatas = datas;
                    }
                }
            }

            return result;
        }

        public CityDetailViewModels GetDetailById(string id)
        {
            var result = new CityDetailViewModels();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("GetCityById", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@cityId", id);

                    // Execute the command
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        // Process the second result set
                        while (reader.Read())
                        {
                            result.Id = reader["Id"].ObjectToString();
                            result.Name = reader["Name"].ObjectToString();
                            result.StateName = reader["StateName"].ObjectToString();
                            result.CountryName = reader["CountryName"].ObjectToString();
                            result.CreatedBy = reader["CreatedBy"].ObjectToString();
                            result.CreatedDate = reader["CreatedDate"].ObjectToDate();
                        }
                    }
                }
            }

            return result;
        }

        public CityModifyViewModels GetDetailToEditById(string id)
        {
            var result = new CityModifyViewModels();
            result.Countries = new List<SelectListItem>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("GetCityById", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@cityId", id);

                    // Execute the command
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        // Process the second result set
                        while (reader.Read())
                        {
                            result.Id = reader["Id"].ObjectToString();
                            result.Name = reader["Name"].ObjectToString();
                            result.CountryId = reader["CountryId"].ObjectToString();
                            result.StateId = reader["StateId"].ObjectToString();
                        }
                    }
                }
            }

            return result;
        }

        public bool SaveNewRecord(SalesModifyViewModels data)
        {
            var result = false;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("InsertNewSale", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    SalesInformation dataToInsert = new SalesInformation
                    {
                        StateId = data.StateId,
                        UpdatedString = "New Record Created By Admin",
                        CountryId = data.CountryId,
                        CreatedBy = "Admin",
                        CreatedDate = DateTime.Now.LocalToUTC(),
                        Id = Guid.NewGuid().ToString(),
                        IsActive = true,
                        DateAndTimeOfSale = DateTime.Now.LocalToUTC(),
                        CityId = data.CityId,
                        CustomerName = data.CustomerName,
                        ProductId = data.ProductId,
                        Quantity = data.Quantity
                    };

                    cmd.Parameters.AddWithValue("@jsonData", dataToInsert.GetJsonFromObject());

                    // Execute the command
                    try
                    {
                        cmd.ExecuteNonQuery();

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                    }
                }
            }

            return result;
        }

        public bool UpdateRecord(CityModifyViewModels data)
        {
            var result = false;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("UpdateCity", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@id", data.Id);
                    cmd.Parameters.AddWithValue("@UpdatedBy", "Admin");
                    cmd.Parameters.AddWithValue("@updatedDate", DateTime.Now.LocalToUTC());
                    cmd.Parameters.AddWithValue("@updateString", "Updated By Admin");
                    cmd.Parameters.AddWithValue("@name", data.Name);
                    cmd.Parameters.AddWithValue("@countryId", data.CountryId);
                    cmd.Parameters.AddWithValue("@stateId", data.StateId);

                    // Execute the command
                    try
                    {
                        cmd.ExecuteNonQuery();

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                    }
                }
            }

            return result;
        }

        public bool DeleteRecord(string id)
        {
            var result = false;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                // Open the connection
                connection.Open();

                // Create a SqlCommand with the stored procedure name
                using (SqlCommand cmd = new SqlCommand("DeleteCity", connection))
                {
                    // Set the command type to stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@id", id);

                    // Execute the command
                    try
                    {
                        cmd.ExecuteNonQuery();

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                    }
                }
            }

            return result;
        }
    }
}