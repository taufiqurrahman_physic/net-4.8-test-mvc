﻿
$(document).ready(function () {
    $('.filter-current-page').off('click').on('click', function (event) {
        event.preventDefault(); // Prevent the default link behavior

        inActiveAllPagination();

        $(this).parent('.page-item').addClass('active');

        var pageClicked = $(this).text();

        HandleFilterPagination(parseInt(pageClicked));
    });

    $('.current-page').off('click').on('click', function (event) {
        event.preventDefault(); // Prevent the default link behavior

        inActiveAllPagination();

        $(this).parent('.page-item').addClass('active');

        var pageClicked = $(this).text();

        let controllerName = $('#controller-name').val();

        HandlePagination(parseInt(pageClicked), controllerName);
    });

    $("#next-page-id").off('click').on('click', function (e) {
        e.preventDefault();

        let lastPage = $("#current-pages").val();

        let controllerName = $('#controller-name').val();

        if (isNaN(lastPage)) {
            setTimeout(() => {
                location.href = '/' + controllerName +'/Index';
            }, 1000);
        }

        HandlePagination(parseInt(lastPage) + 1, controllerName);

    });

    $("#filter-next-page-id").off('click').on('click', function (e) {
        e.preventDefault();

        let lastPage = $("#CurrentPage").val();
        
        if (isNaN(lastPage)) {
            return;
        }

        HandleFilterPagination(parseInt(lastPage) + 1);

    });

    $("#prev-page-id").off('click').on('click', function (e) {
        e.preventDefault();

        let lastPage = $("#current-pages").val();

        let controllerName = $('#controller-name').val();

        if (isNaN(lastPage)) {
            setTimeout(() => {
                location.href = '/' + controllerName +'/Index';
            }, 1000);
        }

        HandlePagination(parseInt(lastPage) - 1, controllerName);

    });

    $("#filter-prev-page-id").off('click').on('click', function (e) {
        e.preventDefault();

        let lastPage = $("#CurrentPage").val();

        if (isNaN(lastPage)) {
            return;
        }

        HandleFilterPagination(parseInt(lastPage) - 1);

    });

});

function inActiveAllPagination() {
    $('.pagination .page-item').removeClass('active');
}

function HandlePagination(currentPage, controllerName) {
        setTimeout(() => {
            location.href = '/' + controllerName +'/Index?currentPage=' + currentPage;
        }, 1000);
}

function HandleFilterPagination(currentPage) {

    
    $('#CurrentPage').val(currentPage);

    $('#search-parameter-form').submit();
}