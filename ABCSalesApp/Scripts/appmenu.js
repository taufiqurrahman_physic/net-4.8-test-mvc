﻿const showNavbar = (navId, bodyId) => {
    const nav = $("#" + navId),
        bodypd = $("#" + bodyId);

    // Validate that all elements exist
    if (nav.length && bodypd.length) {
        nav.on('mouseenter', function () {
            // Show/hide navbar
            nav.toggleClass('show');
            // Change icon
            toggle.toggleClass('bx-x');
            // Add/remove padding to body
            bodypd.toggleClass('body-pd');
        });

        nav.on('mouseleave', function () {
            // Show/hide navbar
            nav.toggleClass('show');
            // Change icon
            toggle.toggleClass('bx-x');
            // Add/remove padding to body
            bodypd.toggleClass('body-pd');
        });
    }
}

showNavbar('nav-bar', 'body-pd');

/*===== LINK ACTIVE =====*/
const linkColor = $('.nav_link');

function colorLink() {
    linkColor.removeClass('active');
    $(this).addClass('active');
}

linkColor.on('click', colorLink);
