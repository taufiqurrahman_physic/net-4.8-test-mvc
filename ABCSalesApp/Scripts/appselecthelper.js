﻿
$(document).ready(function () {

    $("#CountryId").on('change', function (e) {
        e.preventDefault();

        SetStatesId($(this).val());

    });

    $("#StateId").on('change', function (e) {
        e.preventDefault();

        SetCitiesId($(this).val());

    });

});

function SetCitiesId(stateId) {
    let countryId = $("#CountryId").val();

    if (!countryId) {
        return;
    }

    if (!stateId) {
        return;
    }

    var options = $('#CityId');
    options.empty();
    options.append($("<option />").val('').text("Select City"));
    $.get("/SelectHelper/GetSelectItemCities?countryId=" + countryId + "&stateId=" + stateId, function (result) {
        
        if (result) {
            $.each(result, function (i) {
                options.append($("<option />").val(result[i].Value).text(result[i].Text));
            });
        }
    });
}

function SetStatesId(countryId) {

    if (!countryId) {
        return;
    }

    var options = $('#StateId');
    options.empty();
    options.append($("<option />").val('').text("Select State"));
    $.get("/SelectHelper/GetSelectItemStates?countryId=" + countryId, function (result) {
        if (result) {
            $.each(result, function (i) {
                options.append($("<option />").val(result[i].Value).text(result[i].Text));
            });
        }
    });
}