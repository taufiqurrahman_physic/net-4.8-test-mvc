﻿using ABCSalesApp.Extension;

namespace ABCSalesApp.Models
{
    public class City : BaseDataEntityModel
    {
        public string Name { get; set; }
        public string StateId { get; set; }
        public string CountryId { get; set; }
    }
}