﻿using ABCSalesApp.Extension;

namespace ABCSalesApp.Models
{
    public class State : BaseDataEntityModel
    {
        public string Name { get; set; }
        public string AreaCode { get; set; }
        public string StateCode { get; set; }
        public string CountryId { get; set; }
    }
}