﻿using ABCSalesApp.Extension;
using System;

namespace ABCSalesApp.Models
{
    public class SalesInformation : BaseDataEntityModel
    {
        public string CustomerName { get; set; }
        public string CountryId { get; set; }
        public string StateId { get;set; }
        public string CityId { get; set; }
        public DateTime DateAndTimeOfSale { get; set; }
        public string ProductId { get; set; }
        public int Quantity { get; set; }
        public bool IsActive { get; set; }
    }
}