﻿using ABCSalesApp.Extension;

namespace ABCSalesApp.Models
{
    public class Product : BaseDataEntityModel
    {
        public string Name { get; set; }
        public int TotalQuantity { get; set; }
        public int StoredQuantity { get; set; }
    }
}