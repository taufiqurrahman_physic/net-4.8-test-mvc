﻿using ABCSalesApp.Extension;

namespace ABCSalesApp.Models
{
    public class Country : BaseDataEntityModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string CallingCode { get; set; }
    }
}