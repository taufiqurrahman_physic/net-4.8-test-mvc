﻿using ABCSalesApp.Extension.AdditionalAttribute;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ABCSalesApp.Models.ViewModels
{
    public class SalesModifyViewModels
    {
        public string Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "This field is required")]
        [MaxLength(200, ErrorMessage = "This field cant more than 200 character")]
        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "This field is required")]
        [MaxLength(100, ErrorMessage = "This field cant more than 100 character")]
        [Display(Name = "Country")]
        public string CountryId { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "This field is required")]
        [MaxLength(100, ErrorMessage = "This field cant more than 100 character")]
        [Display(Name = "State")]
        public string StateId { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "This field is required")]
        [MaxLength(100, ErrorMessage = "This field cant more than 100 character")]
        [Display(Name = "City")]
        public string CityId { get; set; }
        [Required( ErrorMessage = "This field is required")]
        [Display(Name = "Date and Time Of Sale")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-ddTHH:mm:ss}")]
        [NotMinimumDate("Please select a valid date and time")]
        public DateTime DateAndTimeOfSale { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "This field is required")]
        [MaxLength(100, ErrorMessage = "This field cant more than 100 character")]
        [Display(Name = "Product")]
        public string ProductId { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Value must be greater than or equal to 1")]
        [Display(Name = "Quantity")]
        public int Quantity { get; set; }

        public IEnumerable<SelectListItem> Countries { get; set; }
        public IEnumerable<SelectListItem> States { get; set; }
        public IEnumerable<SelectListItem> Cities { get; set; }
        public IEnumerable<SelectListItem> Products { get; set; }
    }
}