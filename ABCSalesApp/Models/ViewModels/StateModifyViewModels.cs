﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ABCSalesApp.Models.ViewModels
{
    public class StateModifyViewModels
    {
        public string Id { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "This field is required")]
        [MaxLength(200, ErrorMessage ="This field cant more than 200 character")]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "This field is required")]
        [Display(Name = "Area Code")]
        [MaxLength(5, ErrorMessage = "This field cant more than 5 character")]
        public string AreaCode { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "This field is required")]
        [Display(Name = "State Code")]
        [MaxLength(5, ErrorMessage = "This field cant more than 5 character")]
        public string StateCode { get; set; }
        [Display(Name = "Country")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "This field is required")]
        [MaxLength(100, ErrorMessage = "This field cant more than 100 character")]
        public string CountryId { get; set; }

        public IEnumerable<SelectListItem> Countries { get; set; }
    }
}