﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCSalesApp.Models.ViewModels
{
    public class PaginationBaseViewModel
    {
        private int _totalIndexInPage = 3;

        public int TotalDatas { get; set; }
        public int TotalPages 
        {
            get
            {
                if (TotalDatas >= this.TotalDataInPages)
                {
                    var result = Convert.ToInt32(Math.Ceiling((double)TotalDatas/ TotalDataInPages));

                    return result;
                }

                return 1;
            }
        }
        public int CurrentPages { get; set; }
        public IEnumerable<int> PageIndexes
        {
            get
            {
                int totalIndexInPages = this.CurrentPages <= _totalIndexInPage ? _totalIndexInPage : this.CurrentPages;

                var result = new List<int>();

                for (int i = 1; i <= _totalIndexInPage; i++)
                {
                    result.Add(totalIndexInPages);
                    totalIndexInPages--;
                }

                return result.OrderBy(s => s);
            }
        }
        public bool DisablPrev
        {
            get
            {
                return this.CurrentPages == 1;
            }
        }

        public bool DisablNext
        {
            get
            {
                return this.CurrentPages == this.TotalPages;
            }
        }

        public int TotalDataInPages = 10;
    }
}