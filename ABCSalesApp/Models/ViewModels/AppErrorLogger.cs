﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCSalesApp.Models.ViewModels
{
    public class AppErrorLogger
    {
        public string Id { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string ExceptionMessage { get; set; }
        public string ExceptionStackTrack { get; set; }
        public DateTime ExceptionLogTime { get; set; }

    }
}