﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ABCSalesApp.Models.ViewModels
{
    public class CityModifyViewModels
    {
        public string Id { get; set; }
        [Display(Name = "Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "This field is required")]
        [MaxLength(200, ErrorMessage = "This field cant more than 200 character")]
        public string Name { get; set; }
        [Display(Name ="Country")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "This field is required")]
        [MaxLength(100, ErrorMessage = "This field cant more than 100 character")]
        public string CountryId { get; set; }
        [Display(Name = "State")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "This field is required")]
        [MaxLength(100, ErrorMessage = "This field cant more than 100 character")]
        public string StateId { get; set; }

        public IEnumerable<SelectListItem> States { get; set; }
        public IEnumerable<SelectListItem> Countries { get; set; }
    }
}