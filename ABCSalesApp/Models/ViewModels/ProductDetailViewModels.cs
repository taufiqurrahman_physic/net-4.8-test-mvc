﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCSalesApp.Models.ViewModels
{
    public class ProductDetailViewModels
    {
        public string Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string Name { get; set; }
        public int TotalQuantity { get; set; }
        public int StoredQuantity { get; set; }
    }
}