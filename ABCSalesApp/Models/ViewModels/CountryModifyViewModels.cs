﻿using System.ComponentModel.DataAnnotations;

namespace ABCSalesApp.Models.ViewModels
{
    public class CountryModifyViewModels
    {
        public string Id { get;set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "This field is required")]
        [MaxLength(200, ErrorMessage = "This field cant more than 200 character")]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "This field is required")]
        [MaxLength(5, ErrorMessage = "This field cant more than 5 character")]
        [Display(Name = "Code")]
        public string Code { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "This field is required")]
        [MaxLength(5, ErrorMessage = "This field cant more than 5 character")]
        [Display(Name = "Calling Code")]
        public string CallingCode { get; set; }
    }
}