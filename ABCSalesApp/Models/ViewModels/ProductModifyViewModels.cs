﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ABCSalesApp;
using ABCSalesApp.Extension.AdditionalAttribute;

namespace ABCSalesApp.Models.ViewModels
{
    public class ProductModifyViewModels
    {
        public string Id { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "This field is required")]
        public string Name { get; set; }
        [Required( ErrorMessage = "This field is required")]
        [Range(1, int.MaxValue, ErrorMessage = "Value must be greater than or equal to 1")]
        [IntGreatherThan(nameof(StoredQuantity))]
        public int TotalQuantity { get; set; }
        public int StoredQuantity { get; set; }
    }
}