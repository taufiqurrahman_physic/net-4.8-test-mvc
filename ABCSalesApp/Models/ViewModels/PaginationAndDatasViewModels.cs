﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCSalesApp.Models.ViewModels
{
    public class PaginationAndDatasViewModels<T> : PaginationBaseViewModel where T : class
    {
        public IEnumerable<T> GetDatas { get; set; }
    }
}