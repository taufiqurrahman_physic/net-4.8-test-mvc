﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCSalesApp.Models.ViewModels
{
    public class SalesPaginationAndDatasViewModels<T> : PaginationBaseViewModel where T : class
    {
        public SalesSearchParameter SalesSearchParameter { get; set; }
        public IEnumerable<T> GetDatas { get; set; }
    }
}