﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCSalesApp.Models.ViewModels
{
    public class StateDetailViewModels
    {
        public string Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string Name { get; set; }
        public string AreaCode { get; set; }
        public string StateCode { get; set; }
        public string CountryName { get; set; }
    }
}