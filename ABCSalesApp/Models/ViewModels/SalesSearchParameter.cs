﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABCSalesApp.Models.ViewModels
{
    public class SalesSearchParameter
    {
        public int CurrentPage { get; set; }
        [Display(Name ="Minimum Date Sale")]
        public DateTime? MinimumSalesDate { get; set; }
        public string MinimumSalesDateText
        {
            get
            {
                if (!this.MinimumSalesDate.HasValue)
                {
                    return string.Empty;
                }

                return this.MinimumSalesDate.Value.ToString("yyyy-MM-dd");
            }
        }
        [Display( Name ="Maximum Date Sale")]
        public DateTime? MaximumSalesDate { get; set; }
        public string MaximumSalesDateText
        {
            get
            {
                if (!this.MaximumSalesDate.HasValue)
                {
                    return string.Empty;
                }

                return this.MaximumSalesDate.Value.ToString("yyyy-MM-dd");
            }
        }
        [Display(Name = "Country")]
        public string CountryId { get; set; }
        [Display(Name = "State")]
        public string StateId { get; set; }
        [Display(Name = "City")]
        public string CityId { get; set; }

        public IEnumerable<SelectListItem> Countries { get; set; }
        public IEnumerable<SelectListItem> States { get; set; }
        public IEnumerable<SelectListItem> Cities { get; set; }


    }
}